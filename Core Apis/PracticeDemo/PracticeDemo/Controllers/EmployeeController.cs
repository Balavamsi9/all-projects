﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PracticeDemo.Entities;
using Microsoft.EntityFrameworkCore;

namespace PracticeDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private prtdbContext ptContext;
        public EmployeeController(prtdbContext _context)
        {
            this.ptContext = _context;
        }
        [HttpGet]
        [Route("fetch")]
        public IActionResult Fetch()
        {
            var Emplist=this.ptContext.Emps.ToList();
            return Ok(Emplist);
        }
        [HttpPost]
        [Route("create")]
        public IActionResult Create(Emp empobj)
        {
            this.ptContext.Emps.AddAsync(empobj);
            this.ptContext.SaveChanges();
            return Ok("Added Succesfully");

        }
    }
}

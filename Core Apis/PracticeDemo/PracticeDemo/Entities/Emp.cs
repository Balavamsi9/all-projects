﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PracticeDemo.Entities
{
    public partial class Emp
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Salary { get; set; }
        public DateTime? CreatedTime { get; set; }

        public virtual Dep IdNavigation { get; set; }
    }
}

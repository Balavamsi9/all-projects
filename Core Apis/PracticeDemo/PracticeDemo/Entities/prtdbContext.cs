﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace PracticeDemo.Entities
{
    public partial class prtdbContext : DbContext
    {
        public prtdbContext()
        {
        }

        public prtdbContext(DbContextOptions<prtdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Dep> Deps { get; set; }
        public virtual DbSet<Emp> Emps { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySQL("Server=localhost;Database=prtdb;user id=root;password=sysadmin;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Dep>(entity =>
            {
                entity.ToTable("dep");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Emp>(entity =>
            {
                entity.ToTable("emp");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CreatedTime).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Salary).HasColumnType("mediumtext");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Emp)
                    .HasForeignKey<Emp>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("emp_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

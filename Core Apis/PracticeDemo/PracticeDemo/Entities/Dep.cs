﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PracticeDemo.Entities
{
    public partial class Dep
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual Emp Emp { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EmployeeDemo.Entities;
using EmployeeDemo.Models;

namespace EmployeeDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private EmployeeDemoContext empContext;
        public EmployeeController(EmployeeDemoContext _Context)
        {
            this.empContext = _Context;
        }
        [HttpGet]
        [Route("fetch")]
        public IActionResult fetch()
        {

            var emplist = this.empContext.Employees.ToList();
            return Ok(emplist);
        }

        [HttpGet]
        [Route("find")]
        public IActionResult find(int id)
        {

            var emplist = this.empContext.Employees.Where(x => x.Id == id).FirstOrDefault();
            return Ok(emplist);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult create(Employee empobj)
        {
            this.empContext.Employees.AddAsync(empobj);
            this.empContext.SaveChanges();
            return Ok(new Response { Message = "Record Created Successfully" });
        }
        [HttpDelete]
        [Route("delete")]
        public IActionResult delete(int id)
        {
            var item = this.empContext.Employees.Where(x => x.Id == id).FirstOrDefault();
            this.empContext.Remove(item);
            this.empContext.SaveChangesAsync();
            return Ok(new Response { Message = "Record Deleted Successfully" });
        }
        [HttpPut]
        [Route("update")]
        public IActionResult update(Employee obj)
        {
            var item = this.empContext.Employees.Where(x => x.Id == obj.Id).FirstOrDefault();
            item.Name = obj.Name;
            item.City = obj.City;
            item.Salary = obj.Salary;
            this.empContext.SaveChangesAsync();
            return Ok(new Response { Message = "Record Updated Successfully" });
        }

    }
}

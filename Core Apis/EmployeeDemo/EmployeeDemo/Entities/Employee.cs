﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeDemo.Entities
{
    public partial class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double? Salary { get; set; }
        public string City { get; set; }
    }
}

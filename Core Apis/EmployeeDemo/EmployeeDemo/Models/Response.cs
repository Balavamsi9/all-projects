﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeDemo.Models
{
    public class Response
    {
        public string Message { get; set; }
    }
}

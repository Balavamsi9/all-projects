﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollegeDemoAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace CollegeDemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private collegedbContext _Context;
        public StudentController(collegedbContext context)
        {
            this._Context = context;
        }
        [HttpGet]
        [Route("fetch")]
        public IActionResult Fetch()
        {
            var list = this._Context.Students.ToList();
            return Ok(list);
        }
        [HttpGet]
        [Route("find")]
        public IActionResult Find(int id)
        {
            var item = this._Context.Students.Where(x => x.StdId == id).FirstOrDefault();
            return Ok(item);
        }
        [HttpPost]
        [Route("create")]
        public IActionResult Create(Student obj)
        {
            this._Context.Students.AddAsync(obj);
            this._Context.SaveChangesAsync();
            return Ok("Student Added Successfully");

        }
        [HttpPut]
        [Route("update")]
        public IActionResult Update(Student obj)
        {
            var item=this._Context.Students.Where(x => x.StdId == obj.StdId).FirstOrDefault();
            item.StdId = obj.StdId;
            item.StdName = obj.StdName;
            item.Age = obj.Age;
            item.City = obj.City;
            item.Joiningdate = obj.Joiningdate;
            item.Logindate = obj.Logindate;
            this._Context.SaveChangesAsync();
            return Ok("Studentd Record Updated Successfully");
        }
        [HttpDelete]
        [Route("delete")]
        public IActionResult Delete(int StdId)
        {
                var item = this._Context.Students.Where(x => x.StdId == StdId).FirstOrDefault();
                this._Context.Remove(item);
                this._Context.SaveChangesAsync();
                return Ok("Studendt Record Deleted Successfully");
            
        }
    }
}

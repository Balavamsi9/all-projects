﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StudentDemo.Entities
{
    public partial class Student
    {
        public int StdId { get; set; }
        public string StdName { get; set; }
        public int? Age { get; set; }
        public string City { get; set; }
        public DateTime? Joiningdate { get; set; }
        public DateTime? Logindate { get; set; }
        public int? Depid { get; set; }

        public virtual Department Dep { get; set; }
    }
}

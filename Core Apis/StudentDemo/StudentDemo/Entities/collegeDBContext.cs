﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace StudentDemo.Entities
{
    public partial class collegeDBContext : DbContext
    {
        public collegeDBContext()
        {
        }

        public collegeDBContext(DbContextOptions<collegeDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Student> Students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySQL("Server=localhost;Database=collegeDB;user id=root;password=sysadmin;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>(entity =>
            {
                entity.HasKey(e => e.Depid)
                    .HasName("PRIMARY");

                entity.ToTable("department");

                entity.Property(e => e.Depname).HasMaxLength(50);
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasKey(e => e.StdId)
                    .HasName("PRIMARY");

                entity.ToTable("students");

                entity.HasIndex(e => e.Depid, "Depid");

                entity.HasIndex(e => e.StdName, "index_Std");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Joiningdate).HasColumnType("date");

                entity.Property(e => e.StdName).HasMaxLength(100);

                entity.HasOne(d => d.Dep)
                    .WithMany(p => p.Students)
                    .HasForeignKey(d => d.Depid)
                    .HasConstraintName("students_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StudentDemo.Entities
{
    public partial class Department
    {
        public Department()
        {
            Students = new HashSet<Student>();
        }

        public int Depid { get; set; }
        public string Depname { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentDemo.Entities;
using Microsoft.EntityFrameworkCore;

namespace StudentDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private collegeDBContext _Context;
        public StudentController(collegeDBContext context)
        {
            this._Context = context;
        }
        [HttpGet]
        [Route("fetch")]
        public IActionResult Fetch()
        {
            var list = this._Context.Students.ToList();
            return Ok(list);
        }
        [HttpGet]
        [Route("find")]
        public IActionResult Find(int Stdid)
        {
            var item = this._Context.Students.Where(x => x.StdId == Stdid).FirstOrDefault();
            return Ok(item);
        }
        [HttpPost]
        [Route("create")]
        public IActionResult Create(Student obj)
        {
            this._Context.Students.AddAsync(obj);
            this._Context.SaveChangesAsync();
            return Ok("Student Successfully Added");
        }
        [HttpPut]
        [Route("update")]
        public IActionResult Update(Student obj)
        {
            try
            {

                var item = this._Context.Students.Where(x => x.StdId == obj.StdId).FirstOrDefault();
                //item.StdId = obj.StdId;
                item.StdName = obj.StdName;
                item.Age = obj.Age;
                item.City = obj.City;
                item.Joiningdate = obj.Joiningdate;
                item.Logindate = obj.Logindate;
                item.Depid = obj.Depid;
                this._Context.SaveChangesAsync();
                return Ok("Student Record Updated Successfully");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
